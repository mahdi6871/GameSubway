﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControl : MonoBehaviour
{

    public float Speed;            
    [SerializeField] private float SpeedStep;        
    [SerializeField] private float MaxSpeed;         
    [SerializeField] private Vector3 NewPlayerPos;   




    public Rigidbody rb;
    public float jumpforce=10f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        transform.position += Vector3.forward * Speed * Time.deltaTime; // Moving player forward

        Ray rayDown = new Ray(transform.position, transform.TransformDirection(Vector3.down));
 
        RaycastHit hitDown;
        int raySize = 2;

        Physics.Raycast(rayDown, out hitDown, raySize); 
        Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * raySize, Color.red); 
        
        
            if (Input.GetKeyDown(KeyCode.A) &&
                hitDown.collider.gameObject.GetComponent<RoadPos>().roadPosition != RoadPos.Position.Left &&
                FindObjectOfType<ObstacleCheck>().ReturnObstacleCheckState(0) != true)
            {
                transform.position -= NewPlayerPos;
            }
            if (Input.GetKeyDown(KeyCode.D) &&
                hitDown.collider.gameObject.GetComponent<RoadPos>().roadPosition != RoadPos.Position.Right &&
                FindObjectOfType<ObstacleCheck>().ReturnObstacleCheckState(1) != true)
            {
                transform.position += NewPlayerPos;
            }
        

        if (Input.GetKeyDown(KeyCode.Space))
            {
            transform.position = transform.position + new Vector3(0,5,0);
            Invoke("game", 0.4f);
        }
    }

   
    


    public void SetSpeed()
    {
        Speed += SpeedStep;
    }

    void game()
    {
        transform.position=transform.position+  new Vector3(0,-4,0);
    }

}
